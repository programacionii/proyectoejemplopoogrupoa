package es.ujaen.prog2.proyectoejemplopoogrupoa;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author admin
 */
public abstract class Persona {
    
    private String dni;
    public String nombre;
    int edad;

    public Persona() {
    }
 

    public Persona(String dni, String nombre, int edad) {
        this.dni = dni;
        this.nombre = nombre;
        this.edad = edad;
    }
    
    public abstract String getIdentificador();

    @Override
    public String toString() {
        return nombre + " - " + dni;
    }
    
    
    
    
}
