/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.proyectoejemplopoogrupoa;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author admin
 */
public class Main {
    
    public static void main(String[] args) {
        
        Alumno alumnoAdrian = new Alumno("all00023", "Informatica", 4, "12345678A", "Adrian Luque", 30);
        EmpleadoPAS pasAdrian = new EmpleadoPAS(23700, "SI", "987654321", "12345678A", "Adrian Luque", 30);
        EmpleadoPDI pdiAdrian = new EmpleadoPDI("alluque", args, "D-151", "12345678A", "Adrian Luque", 30);
        
        
        System.out.println(alumnoAdrian.getIdentificador());
        System.out.println(pasAdrian.getIdentificador());
        System.out.println(pdiAdrian.getIdentificador());
        
       
        
        ArrayList<EmpleadoPDI> empleados = new ArrayList<>();
        empleados.add(new EmpleadoPDI("glluque", args, "D-151", "12345678A", "Adrian Luque", 30));
        empleados.add(new EmpleadoPDI("blluque", args, "D-151", "12345678A", "Adrian Luque", 30));
        empleados.add(new EmpleadoPDI("alluque", args, "D-151", "12345678A", "Adrian Luque", 30));
        empleados.add(new EmpleadoPDI("zlluque", args, "D-151", "12345678A", "Adrian Luque", 30));
        empleados.add(new EmpleadoPDI("flluque", args, "D-151", "12345678A", "Adrian Luque", 30));
        empleados.add(new EmpleadoPDI("xlluque", args, "D-151", "12345678A", "Adrian Luque", 30));
        
        Collections.sort(empleados);
        
        for (int i = 0; i < empleados.size(); i++) {
            System.out.println(empleados.get(i).getIdentificador());
        }
        
        
    }
  
    
    
    
}
