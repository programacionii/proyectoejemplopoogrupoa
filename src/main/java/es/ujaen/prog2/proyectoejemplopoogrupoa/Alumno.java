/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.proyectoejemplopoogrupoa;

/**
 *
 * @author admin
 */
public class Alumno extends Persona{
    
    String identificadorAlumno;
    String grado;
    int cursoActual;

    public Alumno(String identificadorAlumno, String grado, int cursoActual, String dni, String nombre, int edad) {
        super(dni, nombre, edad);
        this.identificadorAlumno = identificadorAlumno;
        this.grado = grado;
        this.cursoActual = cursoActual;
    }

    @Override
    public String getIdentificador() {
        return identificadorAlumno;
    }
    
    
        
    
}
