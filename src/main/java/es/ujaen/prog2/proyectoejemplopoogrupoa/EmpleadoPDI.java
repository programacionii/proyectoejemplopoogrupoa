/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.proyectoejemplopoogrupoa;

import java.io.Serializable;

/**
 *
 * @author admin
 */
public class EmpleadoPDI  extends Persona implements Comparable<EmpleadoPDI>{
    
    String identificadorPDI;
    String asignaturas[];
    String despacho;

    public EmpleadoPDI(String identificadorPDI, String[] asignaturas, String despacho, String dni, String nombre, int edad) {
        super(dni, nombre, edad);
        this.identificadorPDI = identificadorPDI;
        this.asignaturas = asignaturas;
        this.despacho = despacho;
    }

    @Override
    public String getIdentificador() {
        return identificadorPDI;
    }

    @Override
    public int compareTo(EmpleadoPDI o) {
        return o.identificadorPDI.compareTo(identificadorPDI);
    }  
    
}
